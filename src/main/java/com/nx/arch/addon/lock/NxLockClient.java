package com.nx.arch.addon.lock;

import com.nx.arch.addon.lock.exception.LockException;

/**
 * @类名称 NxLockClient.java
 * @类描述 锁客户端
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午4:14:32
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public interface NxLockClient {
    
    /**
     * @方法名称 build
     * @功能描述 获取锁客户端
     * @return 锁客户端对象
     * @throws Exception 创建异常
     */
    NxLockClient build()
        throws Exception;
    
    /**
     * 
     * @方法名称 newLock
     * @功能描述 获取锁
     * @param lockKey 锁名称，全局唯一标识
     * @return true-获取锁，false-未获得锁
     * @throws LockException 锁异常
     */
    NxLock newLock(String lockKey)
        throws LockException;
    
    /**
     * @方法名称 newLock
     * @功能描述 获取锁
     * @param lockKey 锁名称，全局唯一标识
     * @param reentrant 是否支持重入
     * @return true-获取锁，false-未获得锁
     * @throws LockException 锁异常
     */
    NxLock newLock(String lockKey, boolean reentrant)
        throws LockException;
    
    /**
     * @方法名称 close
     * @功能描述 释放锁
     * @throws LockException 锁异常
     */
    void close()
        throws LockException;
}

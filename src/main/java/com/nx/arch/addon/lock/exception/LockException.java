package com.nx.arch.addon.lock.exception;

import com.nx.arch.addon.lock.client.EtcdResponse;

/**
 * @类名称 LockException.java
 * @类描述 锁异常对象
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午3:40:01
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public class LockException extends Exception {
    private static final long serialVersionUID = 1L;
    
    final Integer httpCode;
    
    final EtcdResponse result;
    
    public EtcdResponse getResult() {
        return result;
    }
    
    public LockException(String message, Throwable cause) {
        super(message, cause);
        this.httpCode = null;
        this.result = null;
    }
    
    public LockException(String message) {
        super(message);
        this.httpCode = null;
        this.result = null;
    }
    
    public LockException(String message, int httpCode) {
        super(message + "(" + httpCode + ")");
        this.httpCode = httpCode;
        this.result = null;
    }
    
    public LockException(String message, EtcdResponse result) {
        super(message);
        this.httpCode = null;
        this.result = result;
    }
    
    public int gethttpCode() {
        return httpCode;
    }
    
    public boolean isHttpError(int httpCode) {
        return (this.httpCode != null && httpCode == this.httpCode);
    }
    
    public boolean isEtcdError(int etcdCode) {
        return (this.result != null && this.result.errorCode != null && etcdCode == this.result.errorCode);
    }
    
    public boolean isNetError() {
        return result == null;
    }
}
